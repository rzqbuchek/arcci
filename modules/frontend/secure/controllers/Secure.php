<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Secure extends SIAT_Controller {

    public function __construct()
    {
        parent::__construct();

        if ( session('id'))
            redirect('admin');
    }

    public function index()
    {
        redirect('secure/login');
    }

    public function login()
    {
        $this->load->view('secure/index');
    }

    public function verifikasi()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Nama Pengguna','required');
        $this->form_validation->set_rules('password','Kata Sandi','required');

        if ($this->form_validation->run() == FALSE)
        {
            set_message('danger',$this->form_validation->first_error());
            redirect('secure/login');
        }
        else
        {   
            $user = post('username');
            $pass = post('password');

            $res = get_where('u_pengguna',array('username'=>$user));

            if ($res->num_rows() == 1)
            {   
                $row = $res->row();

                if( $row->aktif != 1)
                {
                    set_message('error','Akun Anda telah dinonaktifkan oleh Admin.');
                    redirect('secure/login');
                }
                elseif (verify_password($pass,$row->password))
                {
                    $all = get_where('u_grup',array('id'=>$row->grup_id))->row('all');

                    set_session('id',$row->id);
                    set_session('grup_id',$row->grup_id);
                    set_session('all',($all == 1));
                    redirect('admin');
                }
            }

            set_message('error','Username atau Password salah.');
            redirect('secure/login');
        }
    }
}