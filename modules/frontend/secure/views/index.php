<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Gampong.ID</title>
<meta name="keywords" content="">
<meta name="description" content="">

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="<?= base_url('assets/global/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/global/font-awesome/css/font-awesome.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('assets/site/login.css'); ?>">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="aceh-service">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <strong>Gampong.ID</strong>
                    </div>
                    <div class="clearfix"></div>
                    <div class="aceh-service-item">
                        <form id="login" action="<?= site_url('secure/verifikasi'); ?>" method="POST">
                            <?= get_message(); ?>
                            <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                  <input type="text" class="form-control" name="username" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                  <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                            </div>

                            <input type="hidden" name="url" value="<?= @$url; ?>"/>
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-unlock"></i> LogIn</button>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="footer">
                        Copyright &copy; Diskominfo dan Persandian Aceh
                    </div>
                </div>                    
            </div>
        </div>
    </div>
</div>

</html>
