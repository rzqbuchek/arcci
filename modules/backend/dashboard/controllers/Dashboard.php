<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Dashboard extends Backend_Controller {

    public function index()
    {
        $this->templates->admin('index');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('secure/login');
    }

    public function e404()
    {
        $this->templates->admin('E404');
    }


}