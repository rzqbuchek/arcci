<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Organisasi extends Backend_Controller {

    public function index()
    {
        $this->templates->admin('organisasi/index');
    }

    public function jsondata()
    {   
        $this->load->library('arc_datatable');

        $query = "SELECT * FROM u_organisasi
            __where__ __order__ __limit_offset__ ";

        $columns = array(
            'nama',
            'deskripsi',
            'website',
            'pimpinan',
            'alamat',

            'tombol' => function($row){
                $return = '';
                
                if (can('update'))
                    $return .= '<a class="btn btn-default btn-xs" href="'.site_url('admin/role/organisasi/add/'.$row->id).'">Ubah</a>';

                if (can('delete'))
                    $return .= '<a class="btn btn-danger btn-xs btn-hapus" href="'.site_url('admin/role/organisasi/delete/'.$row->id).'">Hapus</a>';

                return $return;
            }
        );

        $this->arc_datatable
             ->set_query($query)
             ->set_column($columns)
             ->get_json();
    }

}