<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Pengguna extends Backend_Controller {


    public function index()
    {
        $this->templates->admin('pengguna/index');
    }

    
    public function add($id = null)
    {   
        if ( ! can('create','update') )
            redirect('admin/pengguna');

        $data = $this->db->get_where('u_pengguna', array('id'=>$id))->row_array();
        $data = get_lastdata($data);

        $data['ls_grup'] = $this->db->get('u_grup')->result();
        $data['ls_orgs'] = $this->db->get('u_organisasi')->result();

        $this->templates->admin('pengguna/form', $data);
    }


    public function save($id = null)
    {
        $this->load->library('form_validation');
        $this->form_validation->unique_reference('id',$id);
        $this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('hp','HP','required');
        $this->form_validation->set_rules('grup_id','Grup','required');
        $this->form_validation->set_rules('organisasi_id','Organisasi','required');
        $this->form_validation->set_rules('username','Username','required|is_unique[u_pengguna.username]');

        if(post('ganti') OR !$id)
            $this->form_validation->set_rules('password','Password','required');

        if ( $this->form_validation->run() === false)
        {
            set_lastdata(post());
            set_message('danger', first_error());
            redirect('admin/role/pengguna/add/'.$id);
        }
        else{

            $data = array(
                'nama_lengkap'  => post('nama_lengkap'),
                'email'         => post('email'),
                'hp'            => post('hp'),
                'grup_id'       => post('grup_id'),
                'organisasi_id' => post('organisasi_id'),
                'deskripsi'     => post('deskripsi'),
                'aktif'         => (int) post('aktif'),

                'username'      =>post('username'),
            );

            if ($id && can('update')){

                if (post('ganti') && can('ganti_password'))
                    $data['password'] = hash_password(post('password'));

                $this->db->update('u_pengguna', $data, array('id'=>$id));
            }
            elseif ( ! $id && can('create')){

                $data['password'] = hash_password(post('password'));
                $this->db->insert('u_pengguna', $data);
            }

            if($this->db->affected_rows())
                set_message('success','Berhasil menyimpan data');

            redirect('admin/role/pengguna');
        }
    }


    public function delete($id = null)
    {
        if ( ! can('delete'))
            redirect('admin/role/pengguna');

        $this->db->delete('u_pengguna', array('id'=>$id));

        if ($this->db->affected_rows())
            set_message('success','Berhasil menghapus data');

        redirect('admin/role/pengguna');
    }


    public function jsondata()
    {   
        $this->load->library('arc_datatable');

        $query = "SELECT p.*, g.nama as grup, o.nama as orgs FROM u_pengguna p
            LEFT JOIN u_grup g ON p.grup_id = g.id
            LEFT JOIN u_organisasi o ON p.organisasi_id = o.id
            __where__ __order__ __limit_offset__ ";

        $columns = array(
            'username(s)',
            'nama_lengkap(s|o)',
            'email(s|o)',
            'hp(s|o)',
            'deskripsi',
            'g.nama as grup(s|o)',
            'o.nama as orgs(s|o)',

            'aktif' => function($row){
                return ($row->aktif == 1) ? 'Aktif':'Non Aktif'; 
            },

            'tombol' => function($row){
                $return = '';
                
                if (can('update'))
                    $return .= '<a class="btn btn-default btn-xs btn-content" href="'.site_url('admin/role/pengguna/add/'.$row->id).'">Ubah</a>';

                if (can('delete'))
                    $return .= '<a class="btn btn-danger btn-xs btn-hapus" href="'.site_url('admin/role/pengguna/delete/'.$row->id).'">Hapus</a>';

                return $return;
            }
        );

        $this->arc_datatable
             ->set_query($query)
             ->set_column($columns)
             ->get_json();
    }


}