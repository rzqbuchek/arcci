<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Level extends Backend_Controller {

    public function index()
    {
        $this->templates->admin('level/index');
    }

    public function add($id = null)
    {   
        if ( ! can('create','update') )
            redirect('admin/moduleone');

        $data = $this->db->get_where('u_grup', array('id'=>$id))->row_array();
        $data = get_lastdata($data);

        $this->templates->admin('level/form', $data);
    }


    public function save($id = null)
    {

        $this->load->library('form_validation');
        $this->form_validation->unique_reference('id',$id);
        $this->form_validation->set_rules('nama','Nama','required|is_unique[u_grup.nama]');
        $this->form_validation->set_rules('deskripsi','Deskripsi','required');

        if ( $this->form_validation->run() === false)
        {
            set_lastdata(post());
            set_message('danger', first_error());
            redirect('admin/role/level/add/'.$id);
        }
        else{

            $data = array(
                'nama'      => post('nama'),
                'deskripsi' => post('deskripsi'),
            );

            if ($id && can('update')){
                $this->db->update('u_grup', $data, array('id'=>$id));
            }
            elseif ( ! $id && can('create')){
                $this->db->insert('u_grup', $data);
            }

            if($this->db->affected_rows())
                set_message('success','Berhasil menyimpan data');

            redirect('admin/role/level');
        }
    }


    public function delete($id = null)
    {
        if ( ! can('delete'))
            redirect('admin/role/level');

        $this->db->delete('u_grup', array('id'=>$id));

        if ($this->db->affected_rows())
            set_message('success','Berhasil menghapus data');

        redirect('admin/moduleone');
    }


    public function jsondata()
    {   
        $this->load->library('arc_datatable');

        $query = "SELECT * FROM u_grup
            __where__ __order__ __limit_offset__ ";

        $columns = array(
            'nama',
            'deskripsi',

            'tombol' => function($row){
                $return = '';
                
                if (can('hak_akses') && ! $row->all)
                    $return .= '<a class="btn btn-default btn-xs" href="'.site_url('admin/role/level/akses/'.$row->id).'">Hak Akses</a>';

                if (can('update'))
                    $return .= '<a class="btn btn-default btn-xs" href="'.site_url('admin/role/level/add/'.$row->id).'">Ubah</a>';

                if (can('delete'))
                    $return .= '<a class="btn btn-danger btn-xs btn-hapus" href="'.site_url('admin/role/level/delete/'.$row->id).'">Hapus</a>';

                return $return;
            }
        );

        $this->arc_datatable
             ->set_query($query)
             ->set_column($columns)
             ->get_json();
    }

    

    public function akses($grup_id = null)
    {
        if ($grup_id == null OR ! can('update')) 
            redirect('admin/role/level');

        $data['grup_id'] = $grup_id;
        $this->templates->admin('level/akses', $data);
    }


    public function jsonakses($grup_id)
    {   
        $this->load->library('arc_datatable');

        $query = "SELECT m.id as idx, m.*, gm.* FROM u_modul m
                LEFT JOIN u_grup_modul gm ON m.id = gm.modul_id AND grup_id = '$grup_id'
                WHERE aktif = 1 
                __where__ __order__ __limit_offset__ ";

        $columns = array(

            'nama' => function($row){
                return '<strong>'.$row->nama.'</strong><br/>'.$row->deskripsi.'<input type="hidden" name="id['.$row->id.']" value="1"/>';
            },

            'can_read'  => function($row){ return '<input type="checkbox" name="r['.$row->id.']" value="1" '.($row->can_read   == 1 ? 'checked':'').'/>';},
            'can_create'=> function($row){ return '<input type="checkbox" name="c['.$row->id.']" value="1" '.($row->can_create == 1 ? 'checked':'').'/>';},
            'can_update'=> function($row){ return '<input type="checkbox" name="u['.$row->id.']" value="1" '.($row->can_update == 1 ? 'checked':'').'/>';},
            'can_delete'=> function($row){ return '<input type="checkbox" name="d['.$row->id.']" value="1" '.($row->can_delete == 1 ? 'checked':'').'/>';},

            'kustom' => function($row) use ($grup_id){
                $res = $this->db->query("
                    SELECT a.id, kustom_aksi, status FROM u_aksi a
                    LEFT JOIN u_grup_aksi g ON a.id = g.aksi_id AND grup_id = '".$grup_id."'
                    WHERE modul_id = '".$row->idx."'")->result();

                $return = '';
                foreach ($res as $r) {
                    $return .= '<input type="checkbox" name="k['.$r->id.']" value="1" '.($r->status == 1 ? 'checked':'').'> '.ucfirst($r->kustom_aksi);
                }

                return $return;
            }
        );

        $this->arc_datatable
             ->set_query($query)
             ->set_column($columns)
             ->get_json();
    }


    public function saveakses($grup_id = null)
    {

        if ($grup_id == null OR ! can('hak_akses'))
            redirect('admin/role/level');

        $c = post('c');
        $r = post('r');
        $u = post('u');
        $d = post('d');

        foreach (post('id') as $k => $v) {

            $this->db->query("
                INSERT IGNORE INTO u_grup_modul (grup_id, modul_id) VALUES ('$grup_id','$k')
                ");
            $this->db->update('u_grup_modul',
                array(
                    'can_create'=> (int) @$c[$k],
                    'can_read'=> (int) @$r[$k],
                    'can_update'=> (int) @$u[$k],
                    'can_delete'=> (int) @$d[$k],
                ),
                array(
                    'grup_id'=>$grup_id,
                    'modul_id'=>$k
                )
            );
        }

        $this->db->delete('u_grup_aksi', array('grup_id'=>$grup_id));
        foreach (post('k') as $k => $v) {
            $this->db->insert('u_grup_aksi', array('grup_id'=>$grup_id, 'aksi_id'=>$k));
        }


        $this->session->unset_userdata('role_modules');
        $this->session->unset_userdata('role_permissions');

        set_message('success','Role akses diubah');
        redirect('admin/role/level/akses/'.$grup_id);
    }

}