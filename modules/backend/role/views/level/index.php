<style>
  .nav-tabs>li>a, .nav-tabs>li>a:hover, .nav-tabs>li>a:focus {
    font-weight: bold;
    border-color: #3c8dbc #3c8dbc #3c8dbc;
    border-radius: 0;
    padding: 8px 15px;
    font-size: 12px;
    background: #3c8dbc;
    color: #ffffff;
  }
  .dataTables_wrapper{
    padding: 0;
  }
  table.dataTable.no-footer{
    margin:0;
  }
  .alert.alert-success {
    margin: 0 0 10px;
  }
  table tr td:last-child{
    text-align: right;
  }
</style>

<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> Level Akses
  </h1>
</section>

<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li><a href="<?= site_url('admin/pengguna'); ?>">Pengguna</a></li>
  <li class="active">Role Akses</li>
</ol>

<section class="content">
  
  <div class="row">
      <div class="col-md-12">
        <?= get_message(); ?>
        <div class="list-all-theme">
          <div class="panel panel-default">
             <div class="panel-heading">
              <h3 class="panel-title">
                <span style="padding:11px;float:left">Daftar Level Akses</span>
                <?php if (can('create')): ?>
                  <span style="padding:5px;float:right">
                    <a class="btn btn-primary btn-content" href="<?= site_url('admin/role/level/add'); ?>"><i class="fa fa-plus"></i> Tambah Level</a>
                  </span>
                <?php endif; ?>
                <div style="clear:both"></div>
              </h3>
            </div>
            <div class="panel-body" style="padding:0">
              <table id="example" class="display top-valign" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th style="width:15px;text-align:right">No.</th>
                    <th style="max-width:150px">Grup</th>
                    <th>Deskripsi</th>
                    <th style="width:150px"></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</section>


<script>
  $(document).ready(function() {
      var table = $('#example').DataTable( {
          "pageLength": 25,
          "dom":"",
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": "<?= site_url('admin/role/level/jsondata'); ?>"
            }
      });
  });
  $(document).on('click','.btn-hapus',function(e){
        e.preventDefault();
        var href=$(this).attr('href');

        swal({
            title: "Anda Yakin?",
            text: "Sistem akan menghapus data ini!",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yakin, hapus saja!",
            cancelButtonText: "Tidak, batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href=href;
            }
        }); 
    });
</script>

