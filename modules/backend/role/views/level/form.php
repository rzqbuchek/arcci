<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> Level Akses
  </h1>
</section>

<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li><a href="<?= site_url('admin/pengguna'); ?>">Pengguna</a></li>
  <li class="active">Role Akses</li>
</ol>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?= get_message(); ?>
            <div class="list-all-theme">
                <form role="form" action="<?= site_url('admin/role/level/save/'.@$id); ?>" method="POST" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= empty(@$id) ? 'Tambah':'Ubah'; ?> Level</h3>
                        </div>
                        <div class="panel-body">
                            
                            <div class="form-group">
                                <label>Level Akses</label>
                                <input type="text" class="form-control" id="nama" name="nama" value="<?= @$nama; ?>" placeholder="Level Akses" style="max-width:400px"/>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?= @$deskripsi; ?>" placeholder="Deskripsi" style="max-width:400px"/>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary" style="margin-right:3px">Simpan</button>
                            <a href="<?= site_url('admin/role/level');?>" class="btn btn-default btn-content" style="margin-right:3px">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>