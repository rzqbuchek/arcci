<style>
  .nav-tabs>li>a, .nav-tabs>li>a:hover, .nav-tabs>li>a:focus {
    font-weight: bold;
    border-color: #3c8dbc #3c8dbc #3c8dbc;
    border-radius: 0;
    padding: 8px 15px;
    font-size: 12px;
    background: #3c8dbc;
    color: #ffffff;
  }
  .dataTables_wrapper{
    padding: 0;
  }
  table.dataTable.no-footer{
    margin:0;
  }
  .alert.alert-success {
    margin: 0 0 10px;
  }
  table tr th, table tr td{
    text-align: center;
  }
  table tr td:nth-child(2), table tr th:nth-child(2){
    text-align: left;
  }
  table tr td:nth-child(7), table tr th:nth-child(7){
    text-align: left;
  }
  .alert.alert-success {
    margin: 15px 15px 0px;
  }
</style>

<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> Hak Akses
  </h1>
</section>

<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li><a href="<?= site_url('admin/pengguna'); ?>">Pengguna</a></li>
  <li class="active">Role Akses</li>
</ol>

<section class="content">
  
  <div class="row">
      <div class="col-md-12">
        <?= get_message(); ?>
        <form action="<?= site_url('admin/role/level/saveakses/'.@$grup_id); ?>" method="POST">
          <div class="list-all-theme">
            <div class="panel panel-default">
               <div class="panel-heading">
                <h3 class="panel-title">
                  <span style="padding:11px;float:left">Hak Level Akses</span>
                  <?php if (can('update')): ?>
                    <span style="padding:5px;float:right">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</a>
                    </span>
                  <?php endif; ?>
                </h3>
                <div style="clear:both"></div>
              </div>
              <div class="panel-body" style="padding:0">
                <table id="example" class="display top-valign" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th style="width:15px;text-align:right">No.</th>
                      <th style="max-width:250px">Modul</th>
                      <th style="width:70px">Tampil (R)</th>
                      <th style="width:70px">Tambah (C)</th>
                      <th style="width:70px">Ubah (U)</th>
                      <th style="width:70px">Hapus (D)</th>
                      <th>Kustom</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </form>
      </div>
  </div>
</section>


<script>
  $(document).ready(function() {
      var table = $('#example').DataTable( {
          "pageLength": 125,
          "dom":"",
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": "<?= site_url('admin/role/level/jsonakses/'.@$grup_id); ?>"
            }
      });
  });
  $(document).on('click','.btn-hapus',function(e){
        e.preventDefault();
        var href=$(this).attr('href');

        swal({
            title: "Anda Yakin?",
            text: "Sistem akan menghapus data ini!",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yakin, hapus saja!",
            cancelButtonText: "Tidak, batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href=href;
            }
        }); 
    });
</script>

