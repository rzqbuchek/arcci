<style>
    #dlength, #dfilter {
        float: left;
    }
    input[type=checkbox], input[type=radio] {
      margin: 2px 5px 0;
      float: left;
  }
</style>

<section class="content-header">
  <h1><i class="fa fa-dashboard"></i> Pengguna</h1>
</section>
<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li class="active">Pengguna</li>
</ol>

<section class="content">
  
  <div class="row">
      <div class="col-md-12">
        <?= get_message(); ?>
        <div class="list-all-theme">
          <div class="panel panel-default">
             <div class="panel-heading">
              <h3 class="panel-title">

                <?php if (can('create')): ?>
                  <span style="padding:5px;float:left">
                    <a class="btn btn-primary btn-content" href="<?= site_url('admin/role/pengguna/add'); ?>"><i class="fa fa-plus"></i> Tambah Data</a>
                  </span>
                <?php endif; ?>

                <div class="pull-right">
                    <div id="dlength"></div>
                    <div id="dfilter"></div>
                </div>
                <div style="clear:both"></div>
              </h3>
            </div>
            <div class="panel-body" style="padding:0">
              <table id="example" class="display top-valign" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th style="width:10px;text-align:right">No.</th>
                    <th>Username</th>
                    <th>Nama Lengkap</th>
                    <th>Email</th>
                    <th>HP</th>
                    <th>Deskripsi</th>
                    <th>Grup</th>
                    <th>Organisasi</th>
                    <th>Status</th>
                    <th style="max-width:100px">Tombol</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</section>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            "pageLength": 25,
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "ajax": {
                "url": "<?= site_url('admin/role/pengguna/jsondata'); ?>",
                }
        });

        $("#dlength").append($(".dataTables_length"));
        $("#dfilter").append($(".dataTables_filter"));
    });

    $(document).on('click','.btn-hapus',function(e){
        e.preventDefault();
        var href=$(this).attr('href');

        swal({
            title: "Anda Yakin?",
            text: "Sistem akan menghapus data ini!",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yakin, hapus saja!",
            cancelButtonText: "Tidak, batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href=href;
            }
        }); 
    });
</script>