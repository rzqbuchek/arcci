<style type="text/css">
    .select2{
        max-width: 300px !important;
    }
</style>

<section class="content-header">
  <h1><i class="fa fa-dashboard"></i> Pengguna</h1>
</section>

<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li class="active">Pengguna</li>
</ol>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?= get_message(); ?>
            <div class="list-all-theme">
                <form role="form" action="<?= site_url('admin/role/pengguna/save/'.@$id); ?>" method="POST" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= empty(@$id) ? 'Tambah':'Ubah'; ?> Pengguna</h3>
                        </div>
                        <div class="panel-body">
                            
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" value="<?= @$nama_lengkap; ?>" placeholder="Nama" style="max-width:400px"/>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" value="<?= @$email; ?>" placeholder="Email" style="max-width:400px"/>
                            </div>
                            <div class="form-group">
                                <label>HP</label>
                                <input type="text" class="form-control" name="hp" value="<?= @$hp; ?>" placeholder="HP" style="max-width:400px"/>
                            </div>

                            <div class="form-group">
                                <label>Grup Akses</label><br/>
                                <select name="grup_id" class="form-control select2" style="width:300px !important">
                                    <option>-- Pilih Level --</option>
                                    <?php 
                                        foreach ($ls_grup as $g) {
                                            $sel = $g->id == @$grup_id ? 'selected':'';
                                            echo '<option value="'.$g->id.'" '.$sel.'>'.$g->nama.'</option>';
                                        }

                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Organisasi</label><br/>
                                <select name="organisasi_id" class="form-control select2" style="width:300px !important">
                                    <option>-- Pilih Organisasi --</option>
                                    <?php 
                                        foreach ($ls_orgs as $o) {
                                            $sel = $o->id == @$organisasi_id ? 'selected':'';
                                            echo '<option value="'.$o->id.'" '.$sel.'>'.$o->nama.'</option>';
                                        }

                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" <?= (@$aktif == 1) ? 'checked':''?> name="aktif" value="1"> Aktif
                                    </label>
                                  </div>
                            </div>

                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" style="max-width:500px" placeholder="Deskripsi" ><?= @$deskripsi; ?></textarea>
                            </div>

                            <hr/>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" value="<?= @$username; ?>" placeholder="Nama" style="max-width:400px"/>
                            </div>
                            <?php if(can('ganti_password')): ?>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" value="" placeholder="Password" style="max-width:400px"/>
                                <?php if(@$id): ?>
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="ganti" value="1"> Ganti Password
                                    </label>
                                  </div>
                                <?php endif; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary" style="margin-right:3px">Simpan</button>
                            <a href="<?= site_url('admin/role/pengguna');?>" class="btn btn-default btn-content" style="margin-right:3px">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $('.select2').select2();
</script>