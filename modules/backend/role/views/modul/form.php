<style type="text/css">
    .tutup{
        display:none;
    }
    .aksis tr td{
        padding-bottom:2px;
    }
</style>
<section class="content-header">
  <h1>
    <i class="fa fa-users"></i> Modul
  </h1>
</section>

<ol class="breadcrumb">
  <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
  <li class="active">Modul</li>
</ol>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?= get_message(); ?>
            <div class="list-all-theme">
                <form role="form" action="<?= site_url('admin/role/modul/save/'.@$id); ?>" method="POST" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?= empty(@$id) ? 'Tambah':'Ubah'; ?> Modul</h3>
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Modul</label>
                                        <input type="text" class="form-control" id="nama" name="nama" value="<?= @$nama; ?>" placeholder="Level Akses"/>
                                    </div>

                                    <div class="form-group">
                                        <label>URL</label>
                                        <input type="text" class="form-control" id="url" name="url" value="<?= @$url; ?>" placeholder="URL"/>
                                    </div>

                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="<?= @$deskripsi; ?>" placeholder="Deskripsi"/>
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox" <?= (@$aktif == 1) ? 'checked':''?> name="aktif" value="1"> Aktif
                                            </label>
                                          </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Kustom Aksi</label>
                                    <table style="width:100%" class="aksis">
                                        <?php foreach ($ls_aksi as $a): ?>
                                            <tr>
                                                <td style="width:35px"><a class="btn btn-danger btn-del"><i class="fa fa-trash"></i></a></td>
                                                <td>
                                                    <input type="text" name="kustom_aksi[<?= @$a->id; ?>]" class="form-control" value="<?= @$a->kustom_aksi; ?>"/>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <tr id="fclone" class="tutup">
                                            <td style="width:35px"><a class="btn btn-danger btn-del"><i class="fa fa-trash"></i></a></td>
                                            <td>
                                                <input type="text" name="kustom_new[]" class="form-control"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <hr/>
                                    <a class="btn btn-warning btnttm btn-xs"><i class="fa fa-plus"></i> Tambah Aksi</a>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary" style="margin-right:3px">Simpan</button>
                            <a href="<?= site_url('admin/role/modul');?>" class="btn btn-default btn-content" style="margin-right:3px">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $('body').on('click','.btnttm', function(){
        $('#fclone').clone().removeClass('tutup').removeAttr('id').insertBefore("#fclone");
    });

    $('body').on('click','.btn-del', function(){
        $(this).parent().parent().remove();
    });
</script>