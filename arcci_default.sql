-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table siat_default.u_aksi
DROP TABLE IF EXISTS `u_aksi`;
CREATE TABLE IF NOT EXISTS `u_aksi` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `modul_id` int(4) NOT NULL,
  `kustom_aksi` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `modul_id` (`modul_id`,`kustom_aksi`),
  CONSTRAINT `FK_usr_kustom_aksi_usr_modul` FOREIGN KEY (`modul_id`) REFERENCES `u_modul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table siat_default.u_aksi: ~2 rows (approximately)
/*!40000 ALTER TABLE `u_aksi` DISABLE KEYS */;
REPLACE INTO `u_aksi` (`id`, `modul_id`, `kustom_aksi`) VALUES
	(1, 2, 'hak_akses'),
	(2, 4, 'ganti_password');
/*!40000 ALTER TABLE `u_aksi` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_grup
DROP TABLE IF EXISTS `u_grup`;
CREATE TABLE IF NOT EXISTS `u_grup` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  `all` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='tabel Grup untuk mengelompokkan pengguna';

-- Dumping data for table siat_default.u_grup: ~3 rows (approximately)
/*!40000 ALTER TABLE `u_grup` DISABLE KEYS */;
REPLACE INTO `u_grup` (`id`, `nama`, `deskripsi`, `all`) VALUES
	(1, 'Admin', 'Admin Aplikasi', 1),
	(2, 'Operator', 'Operator Aplikasi', 0),
	(3, 'Pimpinan', 'Pimpinan', 0);
/*!40000 ALTER TABLE `u_grup` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_grup_aksi
DROP TABLE IF EXISTS `u_grup_aksi`;
CREATE TABLE IF NOT EXISTS `u_grup_aksi` (
  `aksi_id` int(4) NOT NULL,
  `grup_id` int(4) NOT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`aksi_id`,`grup_id`),
  KEY `FK_usr_kustom_grup_usr_grup` (`grup_id`),
  CONSTRAINT `FK_usr_kustom_grup_usr_grup` FOREIGN KEY (`grup_id`) REFERENCES `u_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_usr_kustom_grup_usr_kustom_aksi` FOREIGN KEY (`aksi_id`) REFERENCES `u_aksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table siat_default.u_grup_aksi: ~1 rows (approximately)
/*!40000 ALTER TABLE `u_grup_aksi` DISABLE KEYS */;
REPLACE INTO `u_grup_aksi` (`aksi_id`, `grup_id`, `status`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `u_grup_aksi` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_grup_modul
DROP TABLE IF EXISTS `u_grup_modul`;
CREATE TABLE IF NOT EXISTS `u_grup_modul` (
  `grup_id` int(4) NOT NULL,
  `modul_id` int(4) NOT NULL,
  `can_read` int(1) NOT NULL DEFAULT '0',
  `can_create` int(1) NOT NULL DEFAULT '0',
  `can_update` int(1) NOT NULL DEFAULT '0',
  `can_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`modul_id`,`grup_id`),
  KEY `FK_tb_access_tb_group` (`grup_id`),
  CONSTRAINT `Akses Grup` FOREIGN KEY (`grup_id`) REFERENCES `u_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Akses Modul` FOREIGN KEY (`modul_id`) REFERENCES `u_modul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabel ini digunakan untuk menggantikan grup_modul saat modul kosong,\r\nbiasanya saat gampong baru dibuat';

-- Dumping data for table siat_default.u_grup_modul: ~4 rows (approximately)
/*!40000 ALTER TABLE `u_grup_modul` DISABLE KEYS */;
REPLACE INTO `u_grup_modul` (`grup_id`, `modul_id`, `can_read`, `can_create`, `can_update`, `can_delete`) VALUES
	(2, 1, 1, 1, 1, 1),
	(2, 2, 1, 1, 1, 1),
	(2, 3, 1, 1, 1, 1),
	(2, 4, 1, 1, 1, 1);
/*!40000 ALTER TABLE `u_grup_modul` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_modul
DROP TABLE IF EXISTS `u_modul`;
CREATE TABLE IF NOT EXISTS `u_modul` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `url` varchar(40) NOT NULL,
  `deskripsi` text NOT NULL,
  `aktif` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='tabel Modul yang terdapat pada backend';

-- Dumping data for table siat_default.u_modul: ~4 rows (approximately)
/*!40000 ALTER TABLE `u_modul` DISABLE KEYS */;
REPLACE INTO `u_modul` (`id`, `nama`, `url`, `deskripsi`, `aktif`) VALUES
	(1, 'Modul', 'admin/role/modul', 'Daftarkan modul disini', 1),
	(2, 'Role Akses', 'admin/role/level', 'Hak Izin Peran', 1),
	(3, 'Organisasi', 'admin/role/organisasi', 'Manajemen Organisasi', 1),
	(4, 'Daftar Pengguna', 'admin/role/pengguna', 'Manajemen Pengguna', 1);
/*!40000 ALTER TABLE `u_modul` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_organisasi
DROP TABLE IF EXISTS `u_organisasi`;
CREATE TABLE IF NOT EXISTS `u_organisasi` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `pimpinan` varchar(255) DEFAULT NULL,
  `alamat` text,
  `parent` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table siat_default.u_organisasi: ~2 rows (approximately)
/*!40000 ALTER TABLE `u_organisasi` DISABLE KEYS */;
REPLACE INTO `u_organisasi` (`id`, `nama`, `deskripsi`, `website`, `pimpinan`, `alamat`, `parent`) VALUES
	(1, 'Diskominfo dan Persandian', '-', 'http://diskominfo.acehprov.go.id', '-', 'Lampieung', NULL),
	(2, 'Bidang TIK', 'Teknologi Informasi dan Komunikasi', '-', 'T. Zulfikar, SE', NULL, 1);
/*!40000 ALTER TABLE `u_organisasi` ENABLE KEYS */;

-- Dumping structure for table siat_default.u_pengguna
DROP TABLE IF EXISTS `u_pengguna`;
CREATE TABLE IF NOT EXISTS `u_pengguna` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(62) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `grup_id` int(6),
  `organisasi_id` int(6) DEFAULT NULL,
  `aktif` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`),
  UNIQUE KEY `user_email` (`email`),
  UNIQUE KEY `user_phone` (`hp`),
  KEY `Pengguna Grup` (`grup_id`),
  KEY `Pengguna Organisasi` (`organisasi_id`),
  CONSTRAINT `Pengguna Grup` FOREIGN KEY (`grup_id`) REFERENCES `u_grup` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Pengguna Organisasi` FOREIGN KEY (`organisasi_id`) REFERENCES `u_organisasi` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Tabel pengguna aplikasi ini dari berbagai level akses';

-- Dumping data for table siat_default.u_pengguna: ~2 rows (approximately)
/*!40000 ALTER TABLE `u_pengguna` DISABLE KEYS */;
REPLACE INTO `u_pengguna` (`id`, `username`, `password`, `nama_lengkap`, `email`, `hp`, `deskripsi`, `grup_id`, `organisasi_id`, `aktif`) VALUES
	(1, 'arradys', '$4rc0d3$d1419OheoW9wruS6pKdF.awIblw9rutT9VO/67bbb8820eed29a49b', 'Fuad Ar-Radhi', 'fuad.arradhi@gmail.com', '085260121210', 'Super Admin', 1, 1, 1),
	(3, 'fuad', '$4rc0d3$f166ausoZBC4TWDycnoC9L5hhaN2kNxPMtQDSf3537b7b9a0a0af43', 'Fuad', 'fuad.arradhi@acehprov.go.id', '085260121212', 'Operator', 2, 1, 0);
/*!40000 ALTER TABLE `u_pengguna` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
