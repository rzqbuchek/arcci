<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAT</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?= base_url('assets/global/bootstrap/css/bootstrap.min.css'); ?>">
  <!--<link rel="stylesheet" href="<?= base_url('assets/global/bootstrap/css/bootstrap-theme.min.css'); ?>">-->
  <link rel="stylesheet" href="<?= base_url('assets/global/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/global/datatables/media/css/jquery.dataTables.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/global/sweetalert/dist/sweetalert.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/global/select2/css/select2.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/global/zebra/css/bootstrap/zebra_datepicker.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/admin/css/AdminLTE.css'); ?>">
  
  <!--font -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- jQuery 2.1.4 -->
  <script src="<?= base_url('assets/global/jquery.js'); ?>"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?= base_url('assets/global/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('assets/global/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?= base_url('assets/global/sweetalert/dist/sweetalert.min.js'); ?>"></script>
  <script src="<?= base_url('assets/global/select2/js/select2.min.js'); ?>"></script>
  <script src="<?= base_url('assets/global/zebra/zebra_datepicker.src.js'); ?>"></script>
  <script src="<?= base_url('assets/admin/js/jquery.form.min.js'); ?>"></script>
  <script src="<?= base_url('assets/admin/js/jquery.nestable.js'); ?>"></script>
  <script src="<?= base_url('assets/admin/js/jscolor.min.js'); ?>"></script>
  <script src="<?= base_url('assets/admin/js/history.js'); ?>"></script>

  <link rel="stylesheet" href="<?= base_url('assets/admin/css/style.css'); ?>">

</head>
<body class="hold-transition skin-green-light sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="txt">SIAT</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <div class="dropdown">
            <button class="tanpa-apa-apa" type="button" data-toggle="dropdown">
              <img src="<?= base_url('assets/images/user.svg'); ?>" class="user-image" alt="User Image" style="width:15px;float:left"> <?= pengguna('nama_lengkap'); ?>
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Profil</a></li>
              <li><a href="<?= site_url('admin/dashboard/logout'); ?>">Logout</a></li>
            </ul>
          </div>
        </ul>
      </div>

    </nav>
  </header>
