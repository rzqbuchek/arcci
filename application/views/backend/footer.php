</div> <!-- content-wrapper -->

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; 2017-<?= date('Y');?> <a href="http://acehprov.go.id" class="cop">Gampong.ID Aceh</a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

<div id="base_url" style="display:none"><?= base_url(); ?></div>
<div id="site_url" style="display:none"><?= site_url(); ?></div>

<script src="<?= base_url('assets/admin/js/app.min.js'); ?>"></script>
<script src="<?= base_url('assets/admin/js/script.js'); ?>"></script>

<div class="loading">
    <div style="margin:15% auto;width:120px;text-align:center;">
        <div class="lds-css ng-scope">
            <div style="width:100%;height:100%" class="lds-double-ring">
                <div></div>
                <div></div>
            </div>
            <style type="text/css">@keyframes lds-double-ring {
              0% {
                -webkit-transform: rotate(0);
                transform: rotate(0);
              }
              100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
              }
            }
            @-webkit-keyframes lds-double-ring {
              0% {
                -webkit-transform: rotate(0);
                transform: rotate(0);
              }
              100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
              }
            }
            @keyframes lds-double-ring_reverse {
              0% {
                -webkit-transform: rotate(0);
                transform: rotate(0);
              }
              100% {
                -webkit-transform: rotate(-360deg);
                transform: rotate(-360deg);
              }
            }
            @-webkit-keyframes lds-double-ring_reverse {
              0% {
                -webkit-transform: rotate(0);
                transform: rotate(0);
              }
              100% {
                -webkit-transform: rotate(-360deg);
                transform: rotate(-360deg);
              }
            }
            .lds-double-ring {
              position: relative;
            }
            .lds-double-ring div {
              position: absolute;
              width: 160px;
              height: 160px;
              top: 20px;
              left: 20px;
              border-radius: 50%;
              border: 12px solid #000;
              border-color: #04b2e7 transparent #04b2e7 transparent;
              -webkit-animation: lds-double-ring 1s linear infinite;
              animation: lds-double-ring 1s linear infinite;
            }
            .lds-double-ring div:nth-child(2) {
              width: 132px;
              height: 132px;
              top: 34px;
              left: 34px;
              border-color: transparent #8a9da9 transparent #8a9da9;
              -webkit-animation: lds-double-ring_reverse 1s linear infinite;
              animation: lds-double-ring_reverse 1s linear infinite;
            }
            .lds-double-ring {
              width: 113px !important;
              height: 113px !important;
              -webkit-transform: translate(-56.5px, -56.5px) scale(0.565) translate(56.5px, 56.5px);
              transform: translate(-56.5px, -56.5px) scale(0.565) translate(56.5px, 56.5px);
            }
            </style></div>
        <p style="font-size: 17px;color: #2db2e7;">Loading ...</p>
    </div>
</div>
</body>
</html>