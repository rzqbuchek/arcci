<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      
      <li class="treeview">
        <a href="<?= site_url('admin/dashboard'); ?>">
          <i class="fa fa-laptop"></i> <span>Beranda</span>
        </a>
      </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-newspaper-o"></i>
          <span>Agregator</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?= site_url('admin/agregrator/website/admin'); ?>">List Website</a></li>
          <li><a href="<?= site_url('admin/agregrator/parser'); ?>">Parser Site</a></li>
          <li><a href="<?= site_url('admin/agregrator'); ?>">Agregator Result</a></li><!-- 
          <li><a href="<?= site_url('admin/agregrator/list_archive'); ?>">List Archive News</a></li>
          <li><a href="<?= site_url('admin/agregrator/list_statistik'); ?>">Statistik & Graph News   </a></li> -->
        </ul>
      </li>

      <li class="header">EKSTRA</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i> <span>Hak Akses</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?= site_url('admin/role/modul'); ?>">Modul</a></li>
          <li><a href="<?= site_url('admin/role/level'); ?>">Level Akses</a></li>
          <li><a href="<?= site_url('admin/role/organisasi'); ?>">Organisasi</a></li>
          <li><a href="<?= site_url('admin/role/pengguna'); ?>">Pengguna</a></li>
        </ul>
      </li>
      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<div class="content-wrapper">