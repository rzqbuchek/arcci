<section class="content-header">
  <h1>
    <i class="fa fa-warning"></i> Warning
  </h1>
</section>

<ol class="breadcrumb">
    <li><a href="<?= site_url('admin'); ?>"><i class="fa fa-laptop"></i> Beranda</a></li>
    <li><a href="<?= site_url('admin'); ?>">Warning</a></li>
    <li class="active">Hak Akses</li>
  </ol>

<section class="content">

  <div class="row">
    <section class="col-lg-12 connectedSortable">

      <div class="box box-success" style="box-shadow:none">
        <div class="box-body table-responsive no-padding">
          <div class="warn">
            <div class="logo-warning"></div>
            <h1>Kesalahan : </h1>
            <p>Halaman tidak ditemukan atau Anda tidak diizinkan mengaksesnya.</p>
          </div>
        </div>
      </div>
    </section>
  </div>

</section>
