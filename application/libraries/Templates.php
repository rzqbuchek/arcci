<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Templates{

    public function admin($page, $data = array(), $return = FALSE)
    {   

        $ci =& get_instance();
        
        if ( ! $return){

            if ( ! $ci->input->is_ajax_request()){
                $ci->load->view('backend/header', $data, $return);
                $ci->load->view('backend/sidebar', $data, $return);
            }
            $ci->load->view($page, $data, $return);

            if ( ! $ci->input->is_ajax_request()){
                $ci->load->view('backend/footer', $data, $return);
            }
        }else{
            $view = '';
            if ( ! $ci->input->is_ajax_request()){
                $view .= $ci->load->view('backend/header', $data, $return);
                $view .= $ci->load->view('backend/sidebar', $data, $return);
            }
            $view .= $ci->load->view($page, $data, $return);

            if ( ! $ci->input->is_ajax_request()){
                $view .= $ci->load->view('backend/footer', $data, $return);
            }

            return $view;
        }
    }

    public function site($page, $data = array(), $return = FALSE)
    {   

        $ci =& get_instance();
        
        if ( ! $return){

            if ( ! $ci->input->is_ajax_request()){
                $ci->load->view('frontend/header', $data, $return);
                $ci->load->view('frontend/sidebar', $data, $return);
            }
            $ci->load->view($page, $data, $return);

            if ( ! $ci->input->is_ajax_request()){
                $ci->load->view('frontend/footer', $data, $return);
            }
        }else{
            $view = '';
            if ( ! $ci->input->is_ajax_request()){
                $view .= $ci->load->view('frontend/header', $data, $return);
                $view .= $ci->load->view('frontend/sidebar', $data, $return);
            }
            $view .= $ci->load->view($page, $data, $return);

            if ( ! $ci->input->is_ajax_request()){
                $view .= $ci->load->view('frontend/footer', $data, $return);
            }

            return $view;
        }
    }

}