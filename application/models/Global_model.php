<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

Class Global_model extends CI_Model {

	public function get_modul($grup_id = null)
	{	

		if ($grup_id == null) 
			$grup_id = session('grup_id');
		
		$modules = session('role_modules');

		if (empty($modules)){
			$modul = $this->db->query("
	            SELECT GROUP_CONCAT(id) as id, GROUP_CONCAT(url) as url, 
	            GROUP_CONCAT(IFNULL(a.can_read,'')) as can_read,
	            GROUP_CONCAT(m.aktif) as aktif
	            FROM u_modul m
				LEFT JOIN u_grup_modul a ON m.id = a.modul_id AND a.grup_id = '$grup_id'")->row();

			$modules = array(
				'id' => $modul->id,
				'url' => $modul->url,
				'can_read' => $modul->can_read,
				'aktif' => $modul->aktif,
			);
			set_session('role_modules', $modules);
		}

		return $modules;
		
	}

	public function permission($modul_id)
	{

		$permission = session('role_permissions');

		if (empty($permission)){
			$perm = $this->db->query("SELECT 
				modul_id, CONCAT(if(can_read = 1, 'read,',''),
				if(can_create = 1, 'create,',''),
				if(can_update = 1, 'update,',''),
				if(can_delete = 1, 'delete,','')) as permissions
				FROM u_grup_modul WHERE grup_id = '".session('grup_id')."'")->result();

			$permission = array();
			foreach ($perm as $p) {

				$perms = explode(',', $p->permissions); 
				$perms = is_array($perms) ? $perms : array();

				$kustom = $this->db->query("SELECT GROUP_CONCAT(kustom_aksi) as kustom FROM u_aksi ka
							LEFT JOIN u_grup_aksi kg ON ka.id = kg.aksi_id
							WHERE modul_id = '".$p->modul_id."' AND grup_id = '".session('grup_id')."'")->row('kustom');

				$kustom = explode(',', $kustom);
				$kustom = is_array($kustom) ? $kustom : array();

				$permission[$p->modul_id] = array_filter(array_merge($perms, $kustom));
			}

			set_session('role_permissions', $permission);
		}
		return @$permission[$modul_id];
	}
}