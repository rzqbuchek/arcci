<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Versi
|--------------------------------------------------------------------------
|
| versi terakhir
|
*/
$config['app_version'] = '1.0.0.v112017';


/*
|--------------------------------------------------------------------------
| ALLOWED MODUL FOR ALL USER
|--------------------------------------------------------------------------
|
| Izinkan modul-modul berikut untuk semua user
|
*/
$config['allowed_modul'] = array(
		'admin','admin/logout','admin/dashboard','admin/reset'
		);


/*
|--------------------------------------------------------------------------
| PATH KE FOLDER UPLOAD MASING-MASING GAMPONG
|--------------------------------------------------------------------------
|
| Merubah bagian ini akan membuat beberapa gambar tidak diakses
| jangan lupa sesuaikan.
|
| With trailing slash
|
*/
// $config['upload_path'] = '/var/www/data/';
$config['upload_path'] = './uploads/';


/*
|--------------------------------------------------------------------------
| PINDAHKAN MODULES KE LUAR APPLICATION
|--------------------------------------------------------------------------
| 
| Memudahkan development untuk tim
| Dipisahkan mulai folder/modul/controller/
|
|
*/
$config['modules_locations'] = array(
        FCPATH.'modules/' => '../../modules/',
        );

