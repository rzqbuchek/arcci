<?php defined('BASEPATH') OR exit('No direct script access allowed');


if ( ! function_exists('first_error'))
{
    function first_error()
    {   
        $ci =& get_instance();
        $ci->load->library('form_validation');
        return $ci->form_validation->first_error();
    }

}

if ( ! function_exists('redirect_js'))
{
    function redirect_js($url)
    {   
        $ci =& get_instance();
        if ($ci->input->is_ajax_request())
        {
            //use both
            echo '<meta http-equiv="refresh" content="0; url='.site_url($url).'">';
            echo '<script type="text/javascript">window.location.href = "'.site_url($url).'"</script>';
        }else{
            redirect($url);
        }
    }

}

if ( ! function_exists('can'))
{
    function can()
    {   
        if (session('all')) return true;
        
        $num = func_num_args();
        if ($num == 0) return false;
        $ci =& get_instance();
        for($x=0;$x<$num;$x++){
            if (in_array(func_get_arg($x), $ci->permission)){
                return true;
            }
        }
        
    }
}


if ( ! function_exists('flashdata'))
{
    function flashdata($key)
    {   
        $ci =& get_instance();
        return $ci->session->flashdata($key);
    }
}


if ( ! function_exists('set_flashdata'))
{
    function set_flashdata($key, $val = null)
    {   
        $ci =& get_instance();
        return $ci->session->set_flashdata($key, $val);
    }
}

if ( ! function_exists('userdata'))
{
    function userdata($key)
    {   
        $ci =& get_instance();
        return $ci->session->userdata($key);
    }
}


if ( ! function_exists('set_userdata'))
{
    function set_userdata($key, $val = null)
    {   
        $ci =& get_instance();
        $ci->session->set_userdata($key, $val);
    }
}

if ( ! function_exists('session'))
{
    function session($key)
    {   
        $ci =& get_instance();
        return $ci->session->userdata($key);
    }
}


if ( ! function_exists('set_session'))
{
    function set_session($key, $val = null)
    {   
        $ci =& get_instance();
        $ci->session->set_userdata($key, $val);
    }
}


if ( ! function_exists('get'))
{
    function get($param = null)
    {   
        $ci =& get_instance();
        return $ci->input->get($param, TRUE);
    }
}


if ( ! function_exists('config'))
{
    function config($param = null)
    {   
        $ci =& get_instance();
        return $ci->config->item($param);
    }
}


if ( ! function_exists('post'))
{
    function post($param = null)
    {   
        $ci =& get_instance();
        return $ci->input->post($param, TRUE);
    }

}


if ( ! function_exists('hash_password'))
{
    function hash_password($pass)
    {   
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {

            $unique = md5(uniqid(rand(), true));
            $unique = substr($unique, 0, 22);

            $salt = '$2y$12$' . $unique;
            $hash = crypt($pass, $salt);
            $hash = substr($hash,-32);
            $pref = substr($unique,0,5);
            $suff = substr($unique,-17);

            return '$4rc0d3$'.$pref.$hash.$suff;;
        }
    }

}


if ( ! function_exists('verify_password'))
{
    function verify_password($pass_us,$pass_db)
    {   
        $pass_db = str_replace('$4rc0d3$','',$pass_db);

        $prefix = substr($pass_db,0,5);
        $suffix = substr($pass_db,-17);
        $unique = $prefix.$suffix;

        $hash = str_replace($prefix,'',$pass_db);
        $hash = str_replace($suffix,'',$hash);
        $hash = '$2y$12$'.substr($unique,0,strlen($unique)-1).$hash;

        $user = crypt($pass_us, '$2y$12$'.$unique);
        return ($user == $hash);
    }

}


if ( ! function_exists('set_message'))
{
    function set_message($type,$msg)
    {   
        $ci =& get_instance();

        if ($ci->input->is_ajax_request()){
            echo '<div class="alert alert-'.$type.' alert-white rounded">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <strong>'.($type == 'success' ? 'Sukses':'Gagal').'!</strong> 
                    '.$msg.'
                 </div>';
            echo '<div class="alert alert-'.$type.'">'.$msg.'</div>';
        }else{
            $ci->session->set_flashdata('flash_message',
                '<div class="alert alert-'.$type.' alert-white rounded">
                    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <strong>'.($type == 'success' ? 'Sukses':'Gagal').'!</strong> 
                    '.$msg.'
                 </div>');
        }
    }

}


if ( ! function_exists('get_message'))
{
    function get_message()
    {   
        $ci =& get_instance();

        return $ci->session->flashdata('flash_message');
    }

}

if ( ! function_exists('get_where'))
{
    function get_where($table, $where)
    {   
        $ci =& get_instance();
        return $ci->db->get_where($table, $where);
    }

}


if ( ! function_exists('set_lastdata'))
{
    function set_lastdata($default)
    {   
        $ci =& get_instance();

        $ci->session->set_flashdata('flash_lastdata',$default);
    }

}


if ( ! function_exists('get_lastdata'))
{
    function get_lastdata($default = array())
    {   
        $ci =& get_instance();

        if (@count($default) > 0){
            $data1 = $default;
        }
        else{
            $data1 = array();
        }

        $data2 = $ci->session->flashdata('flash_lastdata');
        if (@count($data2) == 0){
            $data2 = array();
        }

        $data = array_merge($data1,$data2);
        return $data;
 
    }

}


if ( ! function_exists('pengguna'))
{
    function pengguna($field)
    {   
        $ci =& get_instance();
        $pengguna = $ci->session->flashdata('pengguna');
        if( empty($pengguna)){
            $pengguna = $ci->db->get_where('u_pengguna',array('id'=>session('id')))->row_array();
        }
        return $pengguna[$field];
    }

}


if ( ! function_exists('csrf_generate'))
{
    function csrf_generate()
    {   
        $ci =& get_instance();
        $list = $ci->session->userdata('csrf_secure');
        $list = is_array($list) ? $list : array();
        
        $count = count($list);
        $count = ($count) > 15 ? $count - 15 : 0; 
        array_splice($list, 0, $count);
        
        $random = substr(str_shuffle(MD5(microtime())), 0, 64);
        array_push($list, $random);
        
        $ci->session->set_userdata('csrf_secure',$list);
        return $random;
    }
}


if ( ! function_exists('date_to_form')){
    function date_to_form($tanggal = NULL, $return_now_if_empty = false){

        if ($tanggal == NULL){
            if ($return_now_if_empty){
                return date('d-m-Y');
            }else{
                return false;
            }
        }
        $tanggal = explode(' ', $tanggal);

        $tgl = explode('-',@$tanggal[0]);
        if (strlen($tgl[0]) == 4){
            $tahun = $tgl[0];
            $bulan = $tgl[1];
            $tggal = $tgl[2];
        }else{
            $tahun = $tgl[2];
            $bulan = $tgl[1];
            $tggal = $tgl[0];
        }

        return $tggal .'-'.$bulan.'-'.$tahun;
    }
}


if ( ! function_exists('date_to_db')){
    function date_to_db($tanggal = NULL){

        if ($tanggal == '00-00-0000' OR $tanggal == NULL) $tanggal = date('d-m-Y');

        $tgl = explode('-',$tanggal);
        if (strlen($tgl[0]) == 2){
            $tahun = $tgl[0];
            $bulan = $tgl[1];
            $tggal = $tgl[2];
        }else{
            $tahun = $tgl[2];
            $bulan = $tgl[1];
            $tggal = $tgl[0];
        }

        return $tggal .'-'.$bulan.'-'.$tahun;
    }
}


if ( ! function_exists('redirect_if')){
    function redirect_if($condition, $if_true, $if_false){

        if ($condition)
            redirect($if_true);
        else
            redirect($if_false);
        
    }
}

function current_full_url()
{
    $ci =& get_instance();

    $url = $ci->config->site_url($ci->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
}