<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Backend_Controller extends SIAT_Controller {

    public $permission = array();

    public function __construct()
    {

        parent::__construct();
        if ( ! session('id')){
           redirect_js('secure/login');
        }

        /**
         * PENGECEKAN PADA BAGIAN INI HANYA PADA LEVEL MODUL SAJA
         * Yang dicek hanya read saja
         * Untuk pengecekan create, update dan delete
         * dilakukan sesuai logika bisnis proses
         */

        $this->load->model('global_model');
        $modules  = $this->global_model->get_modul();
        $mid      = explode(',',$modules['id']);
        $modul    = explode(',',$modules['url']);
        $can_read = explode(',',$modules['can_read']);
        $aktif    = explode(',',$modules['aktif']);

        $allow = $this->config->item('allowed_modul');
        $modul = array_merge($modul,$allow);

        $status = false;
        $mcount = 0;
        $iterate= 0;
        $mod_id = 0;
        foreach ($modul as $mdls):
            $mdls  = trim($mdls);
            $mdl   = trim($mdls,'/');
            $mdl   = explode('/',$mdl);
            $count = count($mdl);
            $page  = '';
            for($x=2;$x<$count+2;$x++){
                if($mdl[$x-2] == '*'){
                    $page .= '*/';
                }else{
                    $page .= $this->uri->segment($x-1).'/';
                }
            }
            $page = trim($page,'/');

            if($page == $mdls):
                if ($count > $mcount){
                    $status = (@$can_read[$iterate] == 1 OR in_array($page, $allow) OR session('all')) && 
                              (@$aktif[$iterate] == 1 OR in_array($page, $allow));
                    $mcount = $count;
                    $mod_id = @$mid[$iterate];
                }
            endif;
            $iterate++;
        endforeach;

        if ( ! $status){
            $this->output->set_status_header('404');
            // 404 halaman admin
            echo $this->templates->admin('backend/E404','',TRUE);
            die();
        }else{
            $this->permission = $this->global_model->permission($mod_id);
        }

    }

    
}